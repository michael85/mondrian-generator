
function randomIinteger(min, max) {
    return Math.floor(Math.random() * (+max - +min) + min)
}

class Point {
    //x;
    //y;

    constructor(x, y) {
        this.x = x;
        this.y = y;
    }
}

//creates a rectangle with two point min being
//the top left and max being the lower right
class Rectangle {
    //min;
    //max;

    constructor(min, max) {
        this.min = min;
        this.max = max;
    }

    get width() {
        return this.max.x - this.min.x;

    }

    get height() {
        return this.max.y - this.min.y;
    }

    divide() {
        this.draw();

        //check regions width and height are not to small
        if (this.width < 100 || this.height < 100) {
            return;
        }

        //location of where to divide by x and y
        var x = randomIinteger(this.min.x, this.max.x);
        var y = randomIinteger(this.min.y, this.max.y);

        //if a region is wider and taller than half of the initaial canvas
        //than divide the reagion into 4 smaller regions
        //
        //else if a region is larger than half of the height or width
        //than divide the larger side into two regions
        if (this.width > (width / 2) && this.height > (height / 2)) {
            //top left
            var rect1 = new Rectangle(new Point(this.min.x, this.min.y), new Point(x, y));
            //top right
            var rect2 = new Rectangle(new Point(x, this.min.y), new Point(this.max.x, y));
            //bottom left
            var rect3 = new Rectangle(new Point(this.min.x, y), new Point(x, this.max.y));
            //bottom right
            var rect4 = new Rectangle(new Point(x, y), new Point(this.max.x, this.max.y));

            rect1.divide();
            rect2.divide();
            rect3.divide();
            rect4.divide();
        } else if (this.width > (width / 2)) {//divide on the x
            //left
            var rect1 = new Rectangle(new Point(this.min.x, this.min.y), new Point(x, this.max.y));
            //right
            var rect2 = new Rectangle(new Point(x, this.min.y), new Point(this.max.x, this.max.y));
            rect1.divide();
            rect2.divide();
        } else if (this.height > (height / 2)) {//divide on the y
            //top
            var rect1 = new Rectangle(new Point(this.min.x, this.min.y), new Point(this.max.x, y));
            //bottom
            var rect2 = new Rectangle(new Point(this.min.x, y), new Point(this.max.x, this.max.y));
            rect1.divide();
            rect2.divide();
        }
    }

    draw() {
        var color = "white";
        var colors = ["red", "blue", "yellow", "lightgrey", "black", "red", "blue", "yellow", "lightgrey", "black"];
        //pick a color
        if (randomIinteger(0, 10) > 7) {
            do {
                color = colors[Math.floor(Math.random() * colors.length)];
            } while (lastColor == color)
            lastColor = color;
        }
        ctx.fillStyle = color;
        ctx.fillRect(this.min.x, this.min.y, this.width, this.height);
        ctx.lineWidth = 4;
        ctx.strokeStyle = 'black';
        ctx.strokeRect(this.min.x, this.min.y, this.width, this.height);
    }
}

var canvas = document.querySelector('canvas');
var width = canvas.width = 1000;
var height = canvas.height = 750;
var ctx = canvas.getContext('2d');
var lastColor = "white";

var initialRectangle = new Rectangle(new Point(0, 0), new Point(width, height));
initialRectangle.divide();

//refresh button
function refresh() {
    ctx.clearRect(0, 0, width, height);
    initialRectangle = new Rectangle(new Point(0, 0), new Point(width, height));
    initialRectangle.divide();
}
//download button
function exportCanvasJPG(fileName) {
    var format = "image/jpeg";
    var imgURL = canvas.toDataURL(format);

    var dlLink = document.createElement('a');
    dlLink.download = fileName;
    dlLink.href = imgURL;
    dlLink.dataset.downloadurl = [format, dlLink.download, dlLink.href].join(':');

    document.body.appendChild(dlLink);
    dlLink.click();
    document.body.removeChild(dlLink);
}

